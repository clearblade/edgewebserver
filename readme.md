# edgeWebServer

The __edgeWebServer__ provides the ability to easily serve static web files from an IoT gateway.

## Usage

### Executing the edgeWebServer 

`edgeWebServer -listeningPort=<PORT_NUMBER> -baseDirectory=<BASE_DIR> -logLevel=<LOG_LEVEL>`

   __*Where*__ 

   __listeningPort__
  * OPTIONAL
  * Defaults to 80
  * The port number on which the web server should listen to 

   __baseDirectory__
  * OPTIONAL
  * Defaults to /srv
  * The base directory where the web server should begin looking for any files that requested to be served

   __logLevel__
  * The level of runtime logging the adapter should provide.
  * Available log levels:
    * fatal
    * error
    * warn
    * info
    * debug
  * OPTIONAL
  * Defaults to __info__


## Setup
---
The edgeWebServer was written in Go and therefore requires Go to be installed (https://golang.org/doc/install).


### Commpilation
In order to compile the edgeWebServer for execution within mLinux, the following steps need to be performed:

 1. Retrieve the adapter source code  
    * ```git clone git@bitbucket.org:clearblade/bnsf-connectivitymanagement.git```
 2. Navigate to the edgeWebServer directory  
    * ```cd edgeWebServer```
 4. Compile the adapter
    * ```GOARCH=arm GOARM=5 GOOS=linux go build```



