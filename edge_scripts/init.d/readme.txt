Instructions for use:

1. Copy edgeWebServer.etc.default file into /etc/default, name the file "edgeWebServer"
2. Copy edgeWebServer.etc.initd file into /etc/init.d, name the file "edgeWebServer"
3. From a terminal prompt, execute the following commands:
	3a. chmod 755 /etc/init.d/edgeWebServer
	3b. chown root:root /etc/init.d/edgeWebServer
	3c. update-rc.d edgeWebServer defaults 85

If you wish to start edgeWebServer, rather than reboot, issue the following command from a terminal prompt:

	/etc/init.d/edgeWebServer start