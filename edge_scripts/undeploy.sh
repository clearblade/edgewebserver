#!/bin/bash

#Remove edgeWebServer from monit
sed -i '/edgeWebServer.pid/{N;N;N;N;d}' /etc/monitrc

#Remove the init.d script
rm /etc/init.d/edgeWebServer

#Remove the default variables file
rm /etc/default/edgeWebServer

#Remove the binary
rm /usr/bin/edgeWebServer

#restart monit
/etc/init.d/monit restart
