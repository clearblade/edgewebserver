#!/bin/bash

#Copy binary to /usr/local/bin
mv edgeWebServer /usr/bin

#Ensure binary is executable
chmod +x /usr/bin/edgeWebServer

#Set up init.d resources so that edgeWebServer is started when the gateway starts
mv edgeWebServer.etc.initd /etc/init.d/edgeWebServer
mv edgeWebServer.etc.default /etc/default/edgeWebServer

#Ensure init.d script is executable
chmod +x /etc/init.d/edgeWebServer

#Remove edgeWebServer from monit in case it was already there
sed -i '/edgeWebServer.pid/{N;N;N;N;d}' /etc/monitrc

#Add the adapter to monit
sed -i '/#  check process apache with pidfile/i \
  check process edgeWebServer with pidfile \/var\/run\/edgeWebServer.pid \
    start program = "\/etc\/init.d\/edgeWebServer start" with timeout 60 seconds \
    stop program  = "\/etc\/init.d\/edgeWebServer stop" \
    depends on edge \
 ' /etc/monitrc

#restart monit
/etc/init.d/monit restart


echo "edgeWebServer Deployed"
