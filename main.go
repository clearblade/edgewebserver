package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/hashicorp/logutils"
)

const (
	platURL = "http://localhost:9000"
	messURL = "localhost:1883"
)

var (
	logLevel      string
	listeningPort int
	baseDirectory string
)

func init() {
	flag.IntVar(&listeningPort, "listeningPort", 80, "The port to listen on for HTTP requests (optional)")
	flag.StringVar(&logLevel, "logLevel", "debug", "The level of logging to use. Available levels are 'debug', 'warn', 'error' (optional)")
	flag.StringVar(&baseDirectory, "baseDirectory", "/srv", "The base directory the web server should serve from (optional)")

}

func usage() {
	log.Printf("Usage: connectivityMgmtAdapter [options]\n\n")
	flag.PrintDefaults()
}

func validateFlags() {
	flag.Parse()

}

func main() {
	fmt.Println("Starting HTTP Server...")

	//Validate the command line flags
	flag.Usage = usage
	validateFlags()

	//Initialize the logging mechanism
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "WARN", "ERROR", "FATAL"},
		MinLevel: logutils.LogLevel(strings.ToUpper(logLevel)),
		Writer:   os.Stdout,
	}
	log.SetOutput(filter)

	log.Printf("[INFO] Server listening on port %d\n", listeningPort)

	http.Handle("/", http.FileServer(http.Dir(baseDirectory)))

	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(listeningPort), nil))
}
